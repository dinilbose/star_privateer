ROOSTER
#######

.. autoclass:: star_privateer.ROOSTER
	:special-members: __init__
	:members: 

.. autofunction:: star_privateer.create_rooster_feature_inputs

